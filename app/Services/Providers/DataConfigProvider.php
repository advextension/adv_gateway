<?php
/**
 * Class DataConfigProvider
 * @author: Denis Medvedevskih d.medvedevskih@velosite.ru
 */

namespace App\Services\Providers;


use Dvt\Config\Config;

class DataConfigProvider
{
    private $config;

    /**
     * DataConfigProvider constructor.
     */
    private function __construct()
    {
        $this->config = Config::make('../config');
    }

    /**
     * Get instance
     *
     * @return DataConfigProvider
     */
    public static function getInstance(): DataConfigProvider
    {
        return new self();
    }

    /**
     * @param String $data
     * @return null
     */
    public function getDataFromConfig(String $data)
    {
        $params = explode('.', $data);
        $result = $this->config;
        foreach ($params as $param) {
            $result = $result->get($param);
        }

        return $result;
    }

    /**
     * @param String $service
     * @param String $action
     * @return string
     */
    public function getActionFromService(String $service, String $action)
    {
        return $this->getDataFromConfig('remote_services.' . $service . '.base_url') . '/' . $this->getDataFromConfig('remote_services.' . $service . '.actions.' . $action);
    }
}