<?php


namespace App\Services\Filter;


use Illuminate\Http\Request;

class Filter
{
    private $params;
    private $request;
    private $include;


    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * New instance
     *
     * @param Request $request
     * @return Filter
     */
    public static function newInstance(Request $request = null)
    {
        $instance = new self();
        $instance->setRequest($request);
        $params = [];
        $include = [];
        if (!is_null($request)) {
            foreach ($request->all() as $key => $item) {
                if ($key == 'include') {
                    $include = explode(",", $item);
                } else {

                    $params[$key] = $item;
                }
            }
        }
        $instance->add($params);
        $instance->setIncludeData($include);

        return $instance;
    }


    public function add(array $params)
    {
        foreach ($params as $key => $item) {
            $this->params[$key] = $item;
        }


        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function clearParams()
    {
        $this->params = [];

        return $this;
    }

    public function get(String $param)
    {
        return !empty($this->params[$param]) ? $this->params[$param] : null;
    }

    /**
     * Get query string
     *
     * @return String
     */
    public function getQueryString()
    {
        $query = '?';
        if (!empty($this->getIncludeData())) {
            $query .= 'include=' . implode(",", $this->getIncludeData());
        }
        if (!empty($this->params)) {
            foreach ($this->params as $key => $item) {
                $query .= '&' . $key . '=' . $item;
            }
        }
        if (!is_null($this->getRequest())) {
            foreach ($this->getRequest()->all() as $key => $item) {
                if (!empty($this->params) && key_exists($key, $this->params)) {
                    continue;
                }

                $query .= '&' . $key . '=' . $item;
            }

        }

        return $query;
    }

    public function setIncludeData(array $include)
    {
        $this->include = $include;
    }

    public function getIncludeData()
    {
        return $this->include;
    }
}