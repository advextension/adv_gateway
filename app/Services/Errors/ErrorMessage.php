<?php
namespace App\Services\Errors;

class ErrorMessage
{
    /** @var int $code */
    protected $code;

    /** @var string $message */
    protected $message;


    /**
     * ErrorMessage constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message = 'Неизвестная ошибка', int $code = 500)
    {
        $this->code = $code;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function asArray(): array
    {
        return [
            'status' => 'error',
            'errors' => [
                [
                    'code' => $this->code,
                    'message' => $this->message
                ]
            ]
        ];
    }
}