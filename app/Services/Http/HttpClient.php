<?php
/**
 * Class HttpClient
 * @author: Denis Medvedevskih d.medvedevskih@velosite.ru
 */

namespace App\Services\Http;


use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;

class HttpClient
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param String $url
     * @param array $config
     * @return array
     */
    public function get(String $url, array $config = []): array
    {
        if (!empty($this->getRequest()->header('authorization')[0])) {
            $config['headers'] = ['Authorization' => $this->getRequest()->header('authorization')];
        }

        $client = new Client($config);
        $result = $client->request('GET', $url);
        $data['body'] = \GuzzleHttp\json_decode($result->getBody()->getContents(), true);
        $data['code'] = $result->getStatusCode();

        return $data;
    }

    /**
     * @param String $url
     * @param array $data
     * @param array $config
     * @return array
     */
    public function post(String $url, array $data, $config = []): array
    {
        if (!empty($this->getRequest()->header('HTTP_AUTHORIZATION')[0])) {
            $config['headers'] = ['Authorization' => $this->getRequest()->header('HTTP_AUTHORIZATION')[0]];
        }
        $client = new Client($config);
        $response = $client->post($url, [
            RequestOptions::JSON => $data
        ]);
        $data = $response->getBody()->getContents();
        $result['body'] = \GuzzleHttp\json_decode($data, true);
        $result['code'] = $response->getStatusCode();

        return $result;
    }

    /**
     * @param String $url
     * @param array $data
     * @param array $config
     * @return array
     */
    public function put(String $url, array $data, $config = []): array
    {
        if (!empty($this->getRequest()->header('HTTP_AUTHORIZATION')[0])) {
            $config['headers'] = ['Authorization' => $this->getRequest()->header('HTTP_AUTHORIZATION')[0]];
        }
        $client = new Client($config);
        $response = $client->put($url, [
            RequestOptions::JSON => $data
        ]);
        $data = $response->getBody()->getContents();
        $result['body'] = \GuzzleHttp\json_decode($data, true);
        $result['code'] = $response->getStatusCode();


        return $result;
    }

    /**
     * @param String $url
     * @param array $config
     * @return array
     */
    public function delete(String $url, array $config = []): array
    {
        if (!empty($this->getRequest()->header('authorization')[0])) {
            $config['headers'] = ['Authorization' => $this->getRequest()->header('authorization')];
        }

        $client = new Client($config);
        $result = $client->request('DELETE', $url);
        $data['body'] = \GuzzleHttp\json_decode($result->getBody()->getContents(), true);
        $data['code'] = $result->getStatusCode();

        return $data;
    }

//    /**
//     * Add include data
//     *
//     * @param array $data
//     * @param array|null $include
//     * @param bool $all
//     * @return mixed
//     */
//    public function addIncludeData(array $data, array $include = null, bool $all = true)
//    {
//
//        if (empty($data['body']['data'])) {
//            return $data;
//        }
//        if (!empty($include)) {
//            foreach ($include as $item) {
//                $outPostIds = [];
//                $cityIds = [];
//                $pIds = [];
//                $metroStationsIds = [];
//                if (!empty($data['body']['data'][0])) {
//                    $resource = $data['body']['data'];
//                } elseif (!empty($data['body']['data'])) {
//                    $resource[0] = $data['body']['data'];
//                } else {
//                    $resource = [];
//                }
//                $c = explode(".", $item);
//
//                foreach ($c as $sub) {
//                    // Include outpostPoint
//                    if ($sub == 'outpostPoint' && !isset($data['body']['included']['outpostPoint'])) {
//                        foreach ($resource as $res) {
//                            if (!empty($res['relationships']['outpostPoint']['data']['id'])) {
//                                $outPostIds[$res['relationships']['outpostPoint']['data']['id']] = $res['relationships']['outpostPoint']['data']['id'];
//                            }
//                        }
//                        if (count($outPostIds) > 0) {
//                            $result = $this->get($this->getConfig()->get('remote_services')->get('geo')->get('geo.outpost-points') . "?page[size]=9999&id=" . implode(",", $outPostIds));
//                            if ($all) {
//                                $data['body']['included'][$sub] = $result['body']['data'];
//                            } else {
//                                $data['body']['included'][$sub] = (count($result['body']['data']) > 1) ? $result['body']['data'] : (isset($result['body']['data'][0]) ? $result['body']['data'][0] : $result['body']['data']);
//                            }
//                            $resource = $result['body']['data'];
//                        }
//                    }
//
//                    // Include city
//                    if ($sub == 'city' && !isset($data['body']['included']['city'])) {
//                        foreach ($resource as $res) {
//                            if (!empty($res['relationships']['city']['data']['id'])) {
//                                $cityIds[$res['relationships']['city']['data']['id']] = $res['relationships']['city']['data']['id'];
//                            }
//                        }
//                        if (count($cityIds) > 0) {
//                            $result = $this->get($this->getConfig()->get('remote_services')->get('geo')->get('geo.cities') . "?page[size]=9999&id=" . implode(",", $cityIds));
//                            if ($all) {
//                                $data['body']['included'][$sub] = $result['body']['data'];
//                            } else {
//                                $data['body']['included'][$sub] = (count($result['body']['data']) > 1) ? $result['body']['data'] : (isset($result['body']['data'][0]) ? $result['body']['data'][0] : $result['body']['data']);
//                            }
//                            $resource = $result['body']['data'];
//                        }
//
//                    }
//
//                    // Include company
//                    if ($sub == 'company' && !isset($data['body']['included']['company'])) {
//                        foreach ($resource as $res) {
//                            if (!empty($res['relationships']['company']['data']['id'])) {
//                                $pIds[] = $res['relationships']['company']['data']['id'];
//                            }
//                        }
//
//                        if (count($pIds) > 0) {
//                            $result = $this->get($this->getConfig()->get('remote_services')->get('company')->get('company.list') . "?page[size]=9999&id=" . implode(",", $pIds));
//                            if ($all) {
//                                $data['body']['included'][$sub] = $result['body']['data'];
//                            } else {
//                                $data['body']['included'][$sub] = (count($result['body']['data']) > 1) ? $result['body']['data'] : (isset($result['body']['data'][0]) ? $result['body']['data'][0] : $result['body']['data']);
//                            }
//                            $resource = $result['body']['data'];
//                        }
//
//                    }
//
//                    // Include metro-stations
//                    if ($sub == 'metro-stations' && !isset($data['body']['included']['metro-stations'])) {
//                        foreach ($resource as $res) {
//                            if (!empty($res['relationships']['metro-stations']['data'])) {
//                                foreach ($res['relationships']['metro-stations']['data'] as $station) {
//                                    $metroStationsIds[] = $station['id'];
//                                }
//
//                            }
//                        }
//                        if (count($metroStationsIds) > 0) {
//                            $result = $this->get($this->getConfig()->get('remote_services')->get('geo')->get('geo.metro-stations') . "?page[size]=9999&id=" . implode(",", $metroStationsIds));
//                            if ($all) {
//                                $data['body']['included'][$sub] = $result['body']['data'];
//                            } else {
//                                $data['body']['included'][$sub] = (count($result['body']['data']) > 1) ? $result['body']['data'] : (isset($result['body']['data'][0]) ? $result['body']['data'][0] : $result['body']['data']);
//                            }
//                            $resource = $result['body']['data'];
//                        }
//                    }
//                }
//            }
//        }
//
//        return $data;
//    }

}