<?php

namespace App\Services\RemoteServices\Advert;

use App\Repository\CompanyRepository;
use App\Services\Filter\Filter;
use Illuminate\Http\Request;
use App\Repository\BannerRepository;

/**
 * Class AdvertService
 * @author: Denis Medvedevskih d.medvedevskih@velosite.ru
 */
class AdvertService
{
    private $companyRepository;
    private $bannerRepository;

    /**
     * AdvertService constructor.
     * @param CompanyRepository $companyRepository
     * @param BannerRepository $bannerRepository
     */
    public function __construct(CompanyRepository $companyRepository, BannerRepository $bannerRepository)
    {
        $this->bannerRepository = $bannerRepository;
        $this->companyRepository = $companyRepository;
    }

    /**
     * List companies
     *
     * @param Request $request
     * @return array
     */
    public function getCompanies(Request $request): array
    {
        $filter = Filter::newInstance($request);
        $data = $this->companyRepository->getList($filter);

        return $data;
    }

    /**
     * Get company
     *
     * @param int $id
     * @return array
     */
    public function getCompanyById(int $id): array
    {
        $data = $this->companyRepository->getById($id);

        return $data;
    }

    /**
     * Create company
     *
     * @param Request $request
     * @return array
     */
    public function createCompany(Request $request): array
    {
        $params = $this->getCompanyParamsFromRequest($request);
        $data = $this->companyRepository->create($params);

        return $data;
    }

    /**
     * Update company by id
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function updateCompany(Request $request, int $id): array
    {
        $params = $this->getCompanyParamsFromRequest($request);
        $data = $this->companyRepository->update($params, $id);

        return $data;
    }

    /**
     * Delete company
     *
     * @param int $company_id
     * @return array
     */
    public function deleteCompany(int $company_id): array
    {
        $data = $this->companyRepository->delete($company_id);

        return $data;
    }

    /**
     * Get banners list
     *
     * @param Request $request
     * @return array
     */
    public function getBanners(Request $request): array
    {
        $filter = Filter::newInstance($request);
        $data = $this->bannerRepository->getList($filter);

        return $data;
    }

    /**
     * Create banners
     *
     * @param Request $request
     * @param int $company_id
     * @return array
     */
    public function createBanners(Request $request,int $company_id): array
    {
        $params = $this->getBannersParams($request);
        $data = $this->bannerRepository->create($params,$company_id);

        return $data;
    }

    /**
     * Delete banner
     *
     * @param int $banner_id
     * @return array
     */
    public function deleteBanners(int $banner_id): array
    {
        return $this->bannerRepository->delete($banner_id);
    }

    /**
     * Get params for company from request data
     *
     * @param Request $request
     * @return array
     */
    private function getCompanyParamsFromRequest(Request $request): array
    {
        $params = $request->all();

        return $params;
    }

    private function getBannersParams(Request $request): array
    {
        return $request->all();
    }
}