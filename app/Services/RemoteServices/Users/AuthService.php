<?php

namespace App\Services\RemoteServices\Users;

use App\Repository\UserRepository;
use Illuminate\Http\Request;


/**
 * Class AuthService
 * @author: Denis Medvedevskih d.medvedevskih@velosite.ru
 */
class AuthService
{
    private $userRepository;

    /**
     * AuthService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Login
     *
     * @param Request $request
     * @return array
     */
    public function auth(Request $request): array
    {
        $params = $this->getParams($request);
        $data = $this->userRepository->login($params);

        return $data;
    }

    /**
     * Logout
     *
     * @return array
     */
    public function logout(): array
    {
        $data = $this->userRepository->logout();

        return $data;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function registration(Request $request): array
    {
        $params = $this->getParams($request);
        $data = $this->userRepository->registration($params);

        return $data;
    }

    /**
     * Get params for request
     *
     * @param Request $request
     * @return array
     */
    private function getParams(Request $request): array
    {
        $params = $request->all();

        return $params;
    }
}