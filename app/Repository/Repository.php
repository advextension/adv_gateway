<?php

namespace App\Repository;


use App\Services\Http\HttpClient;

abstract class Repository
{
    protected $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

}