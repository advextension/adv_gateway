<?php
/**
 * Class BannerRepository
 * @author: Denis Medvedevskih d.medvedevskih@velosite.ru
 */

namespace App\Repository;

use App\Services\Filter\Filter;
use App\Services\Providers\DataConfigProvider;

class BannerRepository extends Repository
{
    /**
     * Banners list
     *
     * @param Filter|null $filter
     * @return array
     */
    public function getList(Filter $filter = null): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('advert', 'banner-CRUD');
        if (!is_null($filter)) {
            $url .= $filter->getQueryString();
        }
        $data = $this->httpClient->get($url);

        return $data;
    }

    /**
     *  Create banner for company
     *
     * @param array $params
     * @param int $company_id
     * @return array
     */
    public function create(array $params, int $company_id): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('advert', 'banner-CRUD') . '/' . $company_id;
        $data = $this->httpClient->post($url, $params);

        return $data;
    }

    /**
     * Delete by banner id
     *
     * @param int $banner_id
     * @return array
     */
    public function delete(int $banner_id): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('advert', 'banner-CRUD') . '/' . $banner_id;
        $data = $this->httpClient->delete($url);

        return $data;
    }
}