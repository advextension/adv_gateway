<?php
/**
 * Class UserRepository
 * @author: Denis Medvedevskih d.medvedevskih@velosite.ru
 */

namespace App\Repository;


use App\Services\Providers\DataConfigProvider;

class UserRepository extends Repository
{
    /**
     * Login
     *
     * @param array $params
     * @return array
     */
    public function login(array $params): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('user', 'login');
        $data = $this->httpClient->post($url, $params);

        return $data;
    }

    /**
     * Logout
     *
     * @return array
     */
    public function logout(): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('user', 'logout');
        $data = $this->httpClient->get($url);

        return $data;
    }

    /**
     * Registration
     *
     * @param array $params
     * @return array
     */
    public function registration(array $params): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('user', 'registration');
        $data = $this->httpClient->post($url, $params);

        return $data;
    }
}