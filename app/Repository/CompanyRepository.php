<?php
/**
 * Class CompanyRepository
 * @author: Denis Medvedevskih d.medvedevskih@velosite.ru
 */

namespace App\Repository;

use App\Services\Filter\Filter;
use App\Services\Providers\DataConfigProvider;

class CompanyRepository extends Repository
{
    /**
     * @param Filter|null $filter
     * @return array
     */
    public function getList(Filter $filter = null): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('advert', 'company-CRUD');

        if (!is_null($filter)) {
            $url .= $filter->getQueryString();
        }
        $data = $this->httpClient->get($url);

        return $data;
    }

    /**
     * Create company
     *
     * @param array $params
     * @return array
     */
    public function create(array $params): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('advert', 'company-CRUD');
        $data = $this->httpClient->post($url, $params);

        return $data;
    }

    /**
     * Update company
     *
     * @param array $params
     * @param int $id
     * @return array
     */
    public function update(array $params, int $id): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('advert', 'company-CRUD') . '/' . $id;
        $data = $this->httpClient->put($url, $params);

        return $data;
    }

    /**
     * @param int $id
     * @return array
     */
    public function delete(int $id): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('advert', 'company-CRUD') . '/' . $id;
        $data = $this->httpClient->delete($url);

        return $data;
    }

    /**
     * @param int $id
     * @return array
     */
    public function getById(int $id): array
    {
        $url = DataConfigProvider::getInstance()->getActionFromService('advert', 'company-CRUD') . '/' . $id;
        $data = $this->httpClient->get($url);

        return $data;
    }
}