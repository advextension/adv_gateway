<?php
/**
 * Class AuthController
 * @author: Denis Medvedevskih d.medvedevskih@velosite.ru
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\RemoteServices\Users\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $authService;

    /**
     * AuthController constructor.
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Login
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $data = $this->authService->auth($request);

        return response()->json($data['body'],$data['code']);
    }

    /**
     * Logout
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        $data = $this->authService->logout();

        return response()->json($data['body'],$data['code']);
    }

    /**
     * Registration
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function registration(Request $request): JsonResponse
    {
        $data = $this->authService->registration($request);

        return response()->json($data['body'],$data['code']);
    }
}