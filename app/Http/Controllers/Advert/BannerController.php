<?php
namespace App\Http\Controllers\Advert;
use App\Http\Controllers\Controller;
use App\Services\RemoteServices\Advert\AdvertService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


/**
 * Class BannerController
 * @author: Denis Medvedevskih d.medvedevskih@velosite.ru
 */
class BannerController extends Controller
{
    private $advertService;

    /**
     * BannerController constructor.
     * @param AdvertService $advertService
     */
    public function __construct(AdvertService $advertService)
    {
        $this->advertService = $advertService;
    }

    /**
     * Get adverts
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $data = $this->advertService->getBanners($request);

        return response()->json($data['body'],$data['code']);
    }

    /**
     * Save banners
     *
     * @param Request $request
     * @param int $company_id
     * @return JsonResponse
     */
    public function store(Request $request,int $company_id): JsonResponse
    {
        $data = $this->advertService->createBanners($request,$company_id);

        return response()->json($data['body'],$data['code']);
    }

    /**
     * Delete banner
     *
     * @param int $banner_id
     * @return JsonResponse
     */
    public function destroy(int $banner_id): JsonResponse
    {
        $data = $this->advertService->deleteBanners($banner_id);

        return response()->json($data['body'],$data['code']);
    }
}