<?php
/**
 * Class CompanyController
 * @author: Denis Medvedevskih d.medvedevskih@velosite.ru
 */

namespace App\Http\Controllers\Advert;


use App\Http\Controllers\Controller;
use App\Services\RemoteServices\Advert\AdvertService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    private $advertService;

    /**
     * BannerController constructor.
     * @param AdvertService $advertService
     */
    public function __construct(AdvertService $advertService)
    {
        $this->advertService = $advertService;
    }

    /**
     * Get companies
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $data = $this->advertService->getCompanies($request);

        return response()->json($data['body'],$data['code']);
    }


    /**
     * Show one company
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $data = $this->advertService->getCompanyById($id);

        return response()->json($data['body'],$data['code']);
    }

    /**
     * Create company advert
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $data = $this->advertService->createCompany($request);

        return response()->json($data['body'],$data['code']);
    }

    /**
     * Update company advert
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $data = $this->advertService->updateCompany($request,$id);

        return response()->json($data['body'],$data['code']);
    }

    /**
     * Destroy company
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $data = $this->advertService->deleteCompany($id);

        return response()->json($data['body'],$data['code']);
    }
}