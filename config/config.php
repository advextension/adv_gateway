<?php

return [
    'remote_services' => [
        'user' => [
            'base_url' => 'localhost:8080',
            'actions' => [
                'login' => '/login',
                'logout' => '/logout',
                'registration' => '/users'
            ]
        ],
        'advert' => [
            'base_url' => 'localhost:8080',
            'actions' => [
                'company-CRUD' => '/company',
                'banner-CRUD' => '/banners',
                'banner-events' => '/banners/event',
            ]
        ]
    ]
];