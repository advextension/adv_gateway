<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** Auth */
$router->get('/logout','Auth\AuthController@logout');
$router->post('/login','Auth\AuthController@login');
$router->post('/registration','Auth\AuthController@registration');

/** Adverts */
$router->get('/company','Advert\CompanyController@index');
$router->get('/company/{id}','Advert\CompanyController@show');
$router->post('/company','Advert\CompanyController@store');
$router->put('/company/{id}','Advert\CompanyController@update');
$router->delete('/company/{id}','Advert\CompanyController@destroy');

$router->get('/banners','Advert\BannerController@index');
